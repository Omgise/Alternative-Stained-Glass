package alternativestainedglass.blocks;

import alternativestainedglass.AlternativeStainedGlass;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import java.util.List;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;

public class MedievalStainedGlass extends Block {
    private IIcon[] icons = new IIcon[16];

    public MedievalStainedGlass() {
        super(Material.glass);
        this.setBlockName(AlternativeStainedGlass.MODID + ":medieval_stained_glass");
        this.setBlockTextureName(AlternativeStainedGlass.MODID + ":medieval_stained_glass_purple");
        this.setCreativeTab(AlternativeStainedGlass.modCreativeTab);
        this.setResistance(0.3F);
        this.setHardness(0.3F);
        this.setStepSound(Block.soundTypeGlass);
        this.setLightOpacity(0);
    }

    @Override
    public boolean canSilkHarvest() {
        return true;
    }

    @Override
    public int quantityDropped(int metadata, int fortune, Random random) {
        return 0;
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public boolean renderAsNormalBlock() {
        return false;
    }

    @Override
    public int getRenderBlockPass() {
        return 1;
    }

    @Override
    public boolean canRenderInPass(int pass) {
        return pass == 1;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister iconRegister) {
        for (int i = 0; i <= 15; i++) {
            this.icons[i] = iconRegister.registerIcon(AlternativeStainedGlass.MODID + ":medieval_stained_glass_" + AlternativeStainedGlass.COLORS[i]);
        }
    }

    @Override
    @SideOnly(Side.CLIENT)
    public IIcon getIcon(int side, int metadata) {
        return this.icons[metadata];
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void getSubBlocks(Item item, CreativeTabs creativeTab, List list) {
        for (int i = 0; i < 16; i++) {
            list.add(new ItemStack(item, 1, i));
        }
    }

    @Override
    public int damageDropped(int metadata) {
        return metadata;
    }
}
