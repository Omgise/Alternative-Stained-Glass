package alternativestainedglass;

import alternativestainedglass.blocks.MedievalStainedGlass;
import alternativestainedglass.blocks.MedievalStainedGlassPane;
import alternativestainedglass.blocks.MedievalStainedGlassPaneHorizontal;
import alternativestainedglass.items.SubtypesModBlockItem;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockPane;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

@Mod(
    modid = "alternative_stained_glass",
    name = "Alternative Stained Glass",
    version = "1.0.3"
)
public class AlternativeStainedGlass {
    public static final String MODID = "alternative_stained_glass";

    @Instance(MODID)
    public static AlternativeStainedGlass instance;

    public static Block medievalStainedGlass;
    public static BlockPane medievalStainedGlassPane;
    public static Block medievalStainedGlassPaneHorizontal;

    public static final String[] COLORS = {
        /*0*/"white",
        /*1*/"orange",
        /*2*/"magenta",
        /*3*/"light_blue",
        /*4*/"yellow",
        /*5*/"lime",
        /*6*/"pink",
        /*7*/"gray",
        /*8*/"light_gray",
        /*9*/"cyan",
        /*10*/"purple",
        /*11*/"blue",
        /*12*/"brown",
        /*13*/"green",
        /*14*/"red",
        /*15*/"black"
    };

    public static final CreativeTabs modCreativeTab = new CreativeTabs(AlternativeStainedGlass.MODID) {
        @Override
        public ItemStack getIconItemStack() {
            return new ItemStack(Item.getItemFromBlock(medievalStainedGlass), 1, 10);
        }

        @Override
        public Item getTabIconItem() {
            return Item.getItemFromBlock(medievalStainedGlass);
        }
    };

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        medievalStainedGlass = new MedievalStainedGlass();
        medievalStainedGlassPane = new MedievalStainedGlassPane();
        medievalStainedGlassPaneHorizontal = new MedievalStainedGlassPaneHorizontal();

        GameRegistry.registerBlock(medievalStainedGlass, SubtypesModBlockItem.class, "medieval_stained_glass");
        GameRegistry.registerBlock(medievalStainedGlassPane, SubtypesModBlockItem.class, "medieval_stained_glass_pane");
        GameRegistry.registerBlock(medievalStainedGlassPaneHorizontal, SubtypesModBlockItem.class, "medieval_stained_glass_pane_horizontal");
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        for (int i = 0; i <= 15; i++) {
            // stained_glass -> medieval_stained_glass
            GameRegistry.addShapelessRecipe(new ItemStack(medievalStainedGlass, 1, i),
                new ItemStack(Blocks.stained_glass, 1, i)
            );
            // medieval_stained_glass -> stained_glass
            GameRegistry.addShapelessRecipe(new ItemStack(Blocks.stained_glass, 1, i),
                new ItemStack(medievalStainedGlass, 1, i)
            );
            // stained_glass_pane -> medieval_stained_glass_pane
            GameRegistry.addShapelessRecipe(new ItemStack(medievalStainedGlassPane, 1, i),
                new ItemStack(Blocks.stained_glass_pane, 1, i)
            );
            // medieval_stained_glass_pane -> stained_glass_pane
            GameRegistry.addShapelessRecipe(new ItemStack(Blocks.stained_glass_pane, 1, i),
                new ItemStack(medievalStainedGlassPane, 1, i)
            );
            // medieval_stained_glass -> medieval_stained_glass_pane
            GameRegistry.addRecipe(new ItemStack(medievalStainedGlassPane, 16, i),
                "AAA",
                "AAA",
                'A', new ItemStack(medievalStainedGlass, 1, i)
            );
            // medieval_stained_glass_pane -> medieval_stained_glass_pane_horizontal
            GameRegistry.addRecipe(new ItemStack(medievalStainedGlassPaneHorizontal, 3, i),
                "AAA",
                'A', new ItemStack(medievalStainedGlassPane, 1, i)
            );
            // medieval_stained_glass_pane_horizontal -> medieval_stained_glass_pane
            GameRegistry.addRecipe(new ItemStack(medievalStainedGlassPane, 3, i),
                "A",
                "A",
                "A",
                'A', new ItemStack(medievalStainedGlassPaneHorizontal, 1, i)
            );
        }
    }
}
