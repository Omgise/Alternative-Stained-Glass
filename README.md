# Alternative Stained Glass

![](resources/logo.png)

## Minecraft Forge 1.7.10 mod

Add 16 colored alternative stained-glass blocks.

- Block
- Pane
- Horizontal Pane

![](resources/all_blocks.png)

![](resources/creative_tab.png)

## Requires

- Minecraft 1.7.10
- Forge 10.13.4.1614
- OptiFine (Tested in [OptiFine 1.7.10 HD U D4](https://optifine.net/adloadx?f=OptiFine_1.7.10_HD_U_D4.jar))

## Development

1. `./gradlew build` on Gradle 4.4.1 (Forge Gradle 1.2.x Plugin Compatible)
2. Release `build/libs/mod.jar`

## Credits

Original textures by elwood612 (MIT License)

[Better Stained Glass - Minecraft Resource Packs - CurseForge](https://www.curseforge.com/minecraft/texture-packs/better-stained-glass)

## License

- MIT License (Programs, Textures)
- Apache License (Gradle build tool)
